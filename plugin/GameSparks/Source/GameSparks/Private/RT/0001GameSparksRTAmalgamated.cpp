#include "../GameSparksPrivatePCH.h"

#if defined(_MSC_VER)
#	include "AllowWindowsPlatformTypes.h"
#endif

#if defined(_MSC_VER)
#	pragma warning(disable: 4530) // concrt.h(313): warning C4530: C++ exception handler used, but unwind semantics are not enabled. Specify /EHsc
#	pragma warning(disable: 4702) // xtree(1826): warning C4702: unreachable code
#endif

#define UI GET_OF_MY_LAWN

// silence warnings on windows

#if defined(_MSC_VER)
#	if !defined(_CRT_SECURE_NO_WARNINGS)
#		define _CRT_SECURE_NO_WARNINGS
#	endif
#	if !defined(_CRT_SECURE_NO_DEPRECATE)
#		define _CRT_SECURE_NO_DEPRECATE
#	endif
#endif

//dependencies
#if defined(WIN32)
#	ifdef _MSC_VER
#		include <eh.h>
#	endif
#else 
# 	define _CRTIMP __declspec(dllimport) _CRTIMP bool __cdecl __uncaught_exception();
#endif

#define MBEDTLS_UNREAL
//#include "../../../../../../../base/src/mbedtls/net.cpp"
#undef inline

#ifdef read
#	undef read
#endif

#ifdef write
#	undef write
#endif

#ifdef close
#	undef close
#endif

#undef MBEDTLS_UNREAL

#if !defined(GS_BUILDING_DLL)
#	define GS_BUILDING_DLL 1
#endif /* !defined(GS_BUILDING_DLL) */

#include "../../../GameSparksBaseSDK/src/GameSparksRT/Commands/ActionCommand.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/Commands/CommandFactory.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/Commands/CustomCommand.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/Commands/LogCommand.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/Commands/Requests/CustomRequest.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/Commands/Requests/LoginCommand.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/Commands/Requests/PingCommand.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/Commands/Results/LoginResult.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/Commands/Results/PingResult.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/Commands/Results/PlayerConnectMessage.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/Commands/Results/PlayerDisconnectMessage.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/Commands/Results/UDPConnectMessage.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/Connection/Connection.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/Connection/FastConnection.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/Connection/ReliableConnection.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/GameSparksRT.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/Proto/LimitedPositionStream.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/Proto/Packet.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/Proto/PositionStream.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/Proto/ReusableBinaryWriter.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/Proto/RTData.Serializer.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/Proto/RTVal.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/RTData.cpp"
#include "../../../GameSparksBaseSDK/src/GameSparksRT/RTSessionImpl.cpp"
#include "../../../GameSparksBaseSDK/src/System/IO/BinaryReader.cpp"
#include "../../../GameSparksBaseSDK/src/System/IO/BinaryWriter.cpp"
#include "../../../GameSparksBaseSDK/src/System/IO/MemoryStream.cpp"
#include "../../../GameSparksBaseSDK/src/System/IO/Stream.cpp"
#include "../../../GameSparksBaseSDK/src/System/Net/Sockets/NetworkStream.cpp"
#include "../../../GameSparksBaseSDK/src/System/Net/Sockets/Socket.cpp"
#include "../../../GameSparksBaseSDK/src/System/Net/Sockets/TLSSocket.cpp"
#include "../../../GameSparksBaseSDK/src/System/Net/Sockets/TcpClient.cpp"
#include "../../../GameSparksBaseSDK/src/System/Net/Sockets/UdpClient.cpp"
#include "../../../GameSparksBaseSDK/src/System/Text/Encoding/UTF8.cpp"
#include "../../../GameSparksBaseSDK/src/System/Threading/Thread.cpp"

#undef PF_MAX

// so that unreal still includes MinWindows.h
#ifdef _WINDOWS_
#	undef _WINDOWS_
// Undo any Windows defines.
#	undef uint8
#	undef uint16
#	undef uint32
#	undef int32
#	undef float
#	undef MAX_uint8
#	undef MAX_uint16
#	undef MAX_uint32
#	undef MAX_int32
#	undef CDECL
#	undef PF_MAX
#	undef PlaySound
#	undef DrawText
#	undef CaptureStackBackTrace
#	undef MemoryBarrier
#	undef DeleteFile
#	undef MoveFile
#	undef CopyFile
#	undef CreateDirectory
#endif

#undef UI

#if defined(_MSC_VER)
#	include "HideWindowsPlatformTypes.h"
#endif
